<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
?>

<?php include_once ('../../../Menu.php');?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BookTitle</title>

    <!-- CSS -->

    <link rel="stylesheet" href="../../../Resource/assets_book/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/css/form-elements.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/css/style.css">

    <script src="../../../Resource/assets_book/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resource/assets_book/bootstrap/js/bootstrap.min.js"></script>
    <script src="../../../Resource/assets_book/js/scripts.js"></script>

    <img src="../../../Resource/assets_book/img/backgrounds/1@2x.jpg" style="position: absolute;
    margin: 0px; padding: 0px;border: medium none; width: 100%; height: 100%;
    z-index: -999999; top: 0px;" >



</head>

<body>
<div id="TopMenuBar" align="right">
    <button type="button" onclick="window.location.href='index.php?Page=1'" class=" btn-primary btn-lg">Active List</button>
    <button type="button" onclick="window.location.href='trashed.php?Page=1'" class=" btn-success btn-lg">Trashed List</button>
</div>
<div class="top-content">
<div class ="container">
            <div class="col-sm-5 col-sm-offset-3 form-box">
                <div class="form-top">
                    <div class="form-top-left">
                        <h3>Book Title</h3>
                        <p>Enter BookTitle and Author name:</p>
                    </div>
                    <div class="form-top-right">
                        <i class="fa fa-book"></i>
                    </div>
                </div>
                <div class="form-bottom">
                    <form role="form" action="store.php" method="post" class="login-form">
                        <div class="form-group">
                            <label class="sr-only" for=Book_Title">Book_Title</label>
                            <input type="text" name="book_title" placeholder="Book_Title..." class="Book_Title form-control" id="book_Title">
                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="author_name">Author_name</label>
                            <input type="text" name="author_name" placeholder="Author_name..." class="author_name form-control" id="author_name">
                        </div>
                        <button type="submit" class="btn">Create</button>
                    </form>
                </div>
            </div>
</div>
</div>
<!-- Javascript -->

</div>
</body>

</html>
<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>