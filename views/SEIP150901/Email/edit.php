<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();
use App\Email\Email;

$objEmail=new Email();
$objEmail->setData($_GET);
$oneData=$objEmail->view("obj");

?>
<!DOCTYPE html>
<html>
<head>
    <title>Email</title>
    <meta name="robots" content="noindex, nofollow">
    <!-- Include CSS File Here -->
    <link rel="stylesheet" href="../../../Resource/assets_email/css/style.css"/>
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
    <!-- Include CSS File Here -->
    <script src="../../../Resource/assets_email/js/jquery.min.js"></script>
    <!--<script type="text/javascript" src="../../../Resource/assets_email/js/login.js"></script>-->
</head>
<body>
<div class="container" >
    <h2>Add Email</h2>
    <div class="main">
        <form role="form" action="store.php" method="post" class="login-form">
            <input type ="hidden" name="id" value="<?php echo $oneData->id?>" >
            <div class="form-group"><i class="fa fa-user"></i>
                <label>Username :</label>
                <input type="text" name="username" value="<?php echo $oneData->username ?>" id="user">
            </div>
            <div class="form-group"><i class="fa fa-inbox"></i>
                <label>Email :</label>
                <input type="email" name="email" value="<?php echo $oneData->email ?>" id="email">
            </div>
            <button type="submit" class="btn">Create</button>
        </form>
    </div>
</div>
</body>
</html>