<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="../../../Resource/assets_book/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/font-awesome/css/font-awesome.min.css">
    <script src="../../../Resource/assets_book/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resource/assets_book/bootstrap/js/bootstrap.min.js"></script>
</head>

Trashed List

<?php
require_once("../../../vendor/autoload.php");
use App\Email\Email;
use App\Message\Message;


$objEmail = new Email();

$allData = $objEmail->trashed("obj");
$serial = 1;
echo "<table border='5px' >";

echo "<th> Serial </th>";
echo "<th> ID </th>";
echo "<th> Username </th>";
echo "<th> Email </th>";
echo "<th> Action </th>";


foreach($allData as $oneData){
    echo "<tr style='height: 40px'>";
    echo "<td>".$serial."</td>";

    echo "<td>".$oneData->id."</td>";
    echo "<td>".$oneData->username."</td>";
    echo "<td>".$oneData->email."</td>";


    echo "<td>";

    echo "<a href='recover.php?id=$oneData->id'><button class='btn btn-success'>Recover</button></a> ";

    echo "<a href='delete.php?id=$oneData->id'><button class='btn btn-danger'>Delete</button></a> ";


    echo "</td>";

    echo "</tr>";

    $serial++;
}

echo "</table>";


?>

</html>