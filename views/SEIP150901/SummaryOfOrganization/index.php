<head>
    <link rel="stylesheet" href="../../../Resource/assets_book/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../Resource/assets_book/font-awesome/css/font-awesome.min.css"
    <script src="../../../Resource/assets_book/js/jquery-1.11.1.min.js"></script>
    <script src="../../../Resource/assets_book/bootstrap/js/bootstrap.min.js"></script>
    <img src="../../../Resource/assets_book/img/backgrounds/1@2x.jpg" style="position: absolute;
    margin: 0px; padding: 0px;border: medium none; width: 100%; height: 100%;
    z-index: -999999; top: 0px;" >

</head>
<style>
    table {
        border-spacing: 0;
        border-collapse: collapse;
        Margin: 50px auto;
        background-color: rgba(11, 42, 68, 0.09);
    }
    td, tr {
        padding: 8px;

    }
    th {
        text-align: center;
        padding: 25px;
    }
</style>

<?php
require_once("../../../vendor/autoload.php");

use App\SummaryOfOrganization\SummaryOfOrganization;
use App\Message\Message;
$objSummaryOfOrganization = new SummaryOfOrganization();
$allData = $objSummaryOfOrganization -> index("obj");
$serial = 1;

echo "<table > ";
echo "<th> Serial </th> <th> ID </th> <th> Organization Name </th> <th> Organization Summary </th><th> Action </th>";
foreach($allData as $oneData){
    echo "<tr>";
    echo "<td> $serial </td>";
    echo "<td> $oneData->id </td>";
    echo "<td> $oneData->org_name </td>";
    echo "<td width=500px;> $oneData->org_summary </td>";

    echo
        "<td>
          <a href='view.php? id=$oneData->id'><button class='btn btn-info'>View </button></a>
          <a href='edit.php? id=$oneData->id'><button class='btn btn-success'>Edit </button></a>
          <a href='trash.php? id=$oneData->id'><button class='btn btn-primary'>Trash </button></a>
          <a href='delete.php? id=$oneData->id'><button class='btn btn-danger'>Delete </button></a>
        </td>
        ";



    echo "</tr>";
    $serial++;
}

echo "</table>";

