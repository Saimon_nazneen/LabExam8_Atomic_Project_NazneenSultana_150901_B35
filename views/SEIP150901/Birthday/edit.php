<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo Message::getMessage();

use App\Birthday\Birthday;

$objBirthday=new Birthday();
$objBirthday->setData($_GET);
$oneData=$objBirthday->view("obj");

?>
<?php include_once ('../../../Menu.php');?>

<!DOCTYPE html>
<html>
<head>
    <title>Birthday</title>
    <meta name="robots" content="noindex, nofollow"/>
    <!------------ Including jQuery Date UI with CSS -------------->
    <script src="../../../Resource/assets_birth/js/jquery-1.10.2.js"></script>
    <script src="../../../Resource/assets_birth/js/jquery-ui.js"></script>
    <link rel="stylesheet" href="../../../Resource/assets_birth/css/jquery-ui.css">
    <!-- jQuery Code executes on Date Format option ----->
    <script src="../../../Resource/assets_birth/js/script.js"></script>
    <link rel="stylesheet" href="../../../Resource/assets_birth/css/style.css">
    <link rel="stylesheet" href="../../../Resource/font-awesome/css/font-awesome.min.css">
    <img src="../../../Resource/assets_book/img/backgrounds/1@2x.jpg" style="position: absolute;
    margin: 0px; padding: 0px;border: medium none; width: 100%; height: 100%;
    z-index: -999999; top: 0px;" >

    <style>
        #TopMenuBar{
            margin: 70px;

        }
    </style>

</head>
<body>
<div id="TopMenuBar">
<div class="container">
    <div class="main">
        <h2>Birthday</h2>
        <div >
            <form action="update.php" method="post">
                <label>Name :</label>
                <input type="text" name="person_name" value="<?php echo $oneData->person_name ?>" id="Name"/>
                <label>Date Of Birth :</label>
                <input type="date"  name="birthdate" value="<?php echo $oneData->birthdate ?>">

                <input type="submit" id="submit" value="Create">
            </form>
        </div>
    </div>
</div>
</div>
</body>
</html>
<script>
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(10).fadeOut();
    $('#message').show().delay(10).fadeIn();
    $('#message').show().delay(1200).fadeOut();
</script>