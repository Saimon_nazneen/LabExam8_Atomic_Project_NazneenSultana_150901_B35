-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2016 at 06:26 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `atomic_project_b35`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(23) NOT NULL,
  `person_name` varchar(100) NOT NULL,
  `birthdate` date NOT NULL,
  `is_deleted` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `person_name`, `birthdate`, `is_deleted`) VALUES
(1, 'Nazneen Sultana Saimon', '1993-02-28', 'No'),
(2, 'Abida Sultana Saila', '2002-02-11', 'No'),
(7, 'fdhg', '0000-00-00', 'No'),
(8, 'bvn', '0000-00-00', '2016-11-19 10:32:10'),
(23, '555555555', '2016-11-05', 'No'),
(24, 'dfgh', '2016-11-09', '2016-11-19 11:46:36');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(12) NOT NULL,
  `book_title` varchar(200) NOT NULL,
  `author_name` varchar(100) NOT NULL,
  `is_deleted` varchar(876) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_title`, `author_name`, `is_deleted`) VALUES
(18, 'dfxjnm ', 'cvnjmb', 'No'),
(19, 'fmhbkg', 'xdvf', '2016-11-19 10:03:08'),
(20, 'hg,kjmn', 'sag', '2016-11-19 10:03:09'),
(21, 'BishadShindu', 'Mir Mosharrof Hosen', '2016-11-19 11:45:57'),
(22, 'Himu', 'Humayun Ahmed', '2016-11-19 10:03:10'),
(23, 'java', 'herberd scheld', '2016-11-19 10:03:11'),
(24, 'agfsv', 'dg', '2016-11-19 11:45:26'),
(25, 'fdhbfg', 'nhb ', '2016-11-19 10:03:12');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(21) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  `city_name` varchar(222) NOT NULL,
  `is_deleted` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `country_name`, `city_name`, `is_deleted`) VALUES
(1, 'bangladesh', '', 'No'),
(2, 'United Kingdom', '', 'No'),
(4, 'FRANCE', 'Strasbourg', '2016-11-19 10:45:12'),
(5, 'FRANCE', 'Avignon', 'No'),
(6, 'AUSTRALIA', 'Sydney', 'No'),
(7, 'AUSTRALIA', 'Perth', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(12) NOT NULL,
  `username` varchar(222) NOT NULL,
  `email` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `email_1`
--

CREATE TABLE IF NOT EXISTS `email_1` (
`id` int(132) NOT NULL,
  `username` varchar(453) NOT NULL,
  `email` varchar(62) NOT NULL,
  `is_deleted` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email_1`
--

INSERT INTO `email_1` (`id`, `username`, `email`, `is_deleted`) VALUES
(1, 'dfh', 's@gmail.com', 'No'),
(2, 'fdgd', 's@gmail.com', '2016-11-19 11:46:25'),
(3, 'hbfgc', 's@gmail.com', 'No'),
(4, 'fgjnd', 's@gmail.com', '2016-11-19 11:06:19');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(23) NOT NULL,
  `name` varchar(100) NOT NULL,
  `sex` varchar(20) NOT NULL,
  `is_deleted` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `sex`, `is_deleted`) VALUES
(1, 'Nazneen sultana', 'Female', 'No'),
(2, 'Abida Sultana', 'Female', 'No'),
(4, 'dhs', 'male', '2016-11-19 11:15:19');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(23) NOT NULL,
  `name` varchar(200) NOT NULL,
  `hobbies` varchar(200) NOT NULL,
  `is_deleted` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobbies`, `is_deleted`) VALUES
(1, 'Nazneen Sultana Saimon', 'Programming', 'No'),
(2, 'Abida Sultana', 'Travelling', 'No'),
(3, 'dsf', 'Array', 'No'),
(4, 'fg', 'Array', '2016-11-19 11:27:20'),
(6, 'fdgs', 'travelling,working', 'No'),
(7, 'gjhhgfc', 'progamming,reading', 'No'),
(8, '', 'Programming,Reading', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`id` int(23) NOT NULL,
  `name` varchar(200) NOT NULL,
  `image` varchar(100) NOT NULL,
  `is_deleted` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `image`, `is_deleted`) VALUES
(3, 'dxf', '1478933346Capture5.JPG', '2016-11-19 11:37:20'),
(6, 'Nazneenrthgvjuk44', '1479698610Capture5.JPG', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summaryoforganization`
--

CREATE TABLE IF NOT EXISTS `summaryoforganization` (
`id` int(23) NOT NULL,
  `org_name` varchar(200) NOT NULL,
  `org_summary` varchar(200) NOT NULL,
  `is_deleted` varchar(111) DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summaryoforganization`
--

INSERT INTO `summaryoforganization` (`id`, `org_name`, `org_summary`, `is_deleted`) VALUES
(1, 'Xponant', 'It''s a Software Firm', 'No'),
(2, 'Bitm', 'BASIS Institute of Technology & Management (BITM) with the support of World Bank. BITM was established with a vision to be a world-class IT institute in Bangladesh for the purpose of enhancing the com', 'No'),
(3, 'vbn', 'fgxj', 'No'),
(4, 'xcbx', 'klbghfdsvzbfd', '2016-11-19 11:39:42');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_1`
--
ALTER TABLE `email_1`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(23) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(21) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(12) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `email_1`
--
ALTER TABLE `email_1`
MODIFY `id` int(132) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(23) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(23) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `id` int(23) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `summaryoforganization`
--
ALTER TABLE `summaryoforganization`
MODIFY `id` int(23) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
