<?php
namespace App\SummaryOfOrganization;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class SummaryOfOrganization extends DB{
    public $id;
    public $org_name	;
    public $org_summary;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('org_name',$postVariableData)) {
            $this->org_name = $postVariableData['org_name'];
        }
        if (array_key_exists('org_summary',$postVariableData)) {
            $this->org_summary = $postVariableData['org_summary'];
        }
    }

    public function store(){

        $arrData = array($this->org_name, $this->org_summary);
        $sql = "Insert INTO summaryoforganization(org_name, org_summary) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method


    public function index($fetchMode='ASSOC'){              //ASSOC = Associative array

        $STH = $this->DBH->query("SELECT * from summaryoforganization where is_deleted='No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;

    }// end of index();


    public function view($fetchMode='ASSOC'){              //ASSOC = Associative array

        $STH = $this->DBH->query('SELECT * from summaryoforganization WHERE id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of index();


    public function update()
    {
        $arrData = array($this->org_name, $this->org_summary);
        $sql = "UPDATE summaryoforganization SET org_name = ?, org_summary =? WHERE id =".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }//end of update method;



    public function delete()
    {
        $sql= "DELETE FROM summaryoforganization WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }// end of permanent delete;


    public function trash(){


        $sql = "Update summaryoforganization SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from summaryoforganization where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();



    public function recover(){

        $sql = "Update summaryoforganization SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();

}



