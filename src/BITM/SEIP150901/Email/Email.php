<?php
namespace App\Email;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;

class Email extends DB
{
    public $id;
    public $username;
    public $email;

    public function __construct()
    {
        parent:: __construct();

    }

    public function setData($postVariableData = NULL)
    {
        if (array_key_exists('id',$postVariableData)) {
            $this->id = $postVariableData['id'];
        }
        if (array_key_exists('username',$postVariableData)) {
            $this->username = $postVariableData['username'];
        }
        if (array_key_exists('email',$postVariableData)) {
            $this->email = $postVariableData['email'];
        }
    }

    public function store(){

        $arrData = array($this->username, $this->email);
        $sql = "Insert INTO email_1(username, email) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);

        $result = $STH->execute($arrData);
        if($result)
            Message::setMessage("DATA has been Inserted Successfully :)");
        else
            Message:: setMessage("Failed! DATA has not been Inserted succecssfully :(");
        Utility::redirect('create.php');

    }// end of store method


    public function index($fetchMode='ASSOC'){              //ASSOC = Associative array

        $STH = $this->DBH->query("SELECT * from email_1 where is_deleted='No'");

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;

    }// end of index();


    public function view($fetchMode='ASSOC'){              //ASSOC = Associative array

        $STH = $this->DBH->query('SELECT * from email_1 WHERE id='.$this->id);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrOneData  = $STH->fetch();
        return $arrOneData;

    }// end of index();



    public function update()
    {
        $arrData = array($this->username, $this->email);
        $sql = "UPDATE email_1 SET username = ?, email =? WHERE id =".$this->id;
        $STH=$this->DBH->prepare($sql);
        $STH->execute($arrData);
        Utility::redirect('index.php');

    }//end of update method;



    public function delete()
    {
        $sql= "DELETE FROM email_1 WHERE id=".$this->id;

        $STH = $this->DBH->prepare($sql);
        $STH->execute();
        Utility::redirect('index.php');

    }// end of permanent delete;


    public function trash(){


        $sql = "Update email_1 SET is_deleted=NOW() where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of trash()



    public function trashed($fetchMode='ASSOC'){
        $sql = "SELECT * from email_1 where is_deleted <> 'No' ";
        $STH = $this->DBH->query($sql);

        $fetchMode = strtoupper($fetchMode);
        if(substr_count($fetchMode,'OBJ') > 0)
            $STH->setFetchMode(PDO::FETCH_OBJ);
        else
            $STH->setFetchMode(PDO::FETCH_ASSOC);

        $arrAllData  = $STH->fetchAll();
        return $arrAllData;


    }// end of trashed();





    public function recover(){

        $sql = "Update email_1 SET is_deleted='No' where id=".$this->id;

        $STH = $this->DBH->prepare($sql);

        $STH->execute();

        Utility::redirect('index.php');

    }// end of recover();


}



